---
title: about
# this follows styles in layouts > page > single
type: page
---

# About

**Scuttlebutt is a decent(ralised) secure gossip platform.**

Like other social platforms, you can send messages to your friends and share posts onto a feed! The cool thing is that the underlying technology here means that messages are passed directly between friends via a peer-to-peer (p2p) [gossip technology](https://en.wikipedia.org/wiki/Gossip_protocol).

As a decentralized social network, "gossip"/"data" is passed from friend to friend, without any central server. It also happens to work offline!

The name, scuttlebutt, came from sea-slang for gossip. Basically, like a watercooler on a ship.

Want to learn more? Check out the [documentation](/docs) or [just get started](/get-started)!

![dancing hermie](/images/gif/large-hermies-dancing.gif)

&nbsp;

&nbsp;
